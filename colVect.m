function [vect] = colVect(v)
[rows, cols] = size(v);
if (cols == 1)
    vect = v;
elseif (rows == 1)
    vect = v';
else
    disp('ERROR: input data not a vector')
    return
end

end