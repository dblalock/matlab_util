function [] = setFigureFontSize(sz)

set(findall(gcf,'type','text'),'fontSize',sz)

end