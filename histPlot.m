

function [binVals, binPositions] = histPlot(data, bins, titleStr, xLabel, yLabel)
    [binVals, binPositions] = hist(data, bins);
    binPositions = .5/bins : 1/bins : 1-.5/bins;
    bar1 = bar(binPositions, binVals);
    
    title(titleStr);
    xlabel(xLabel);
    ylabel(yLabel);
    
    
    % Create textbox
%     annotation1 = annotation(bar1,...
%     'textbox' ,...
%     'Position' ,[0.6643 0.9405 0.308 0.07024],...
%     'LineStyle','none',...
%     'String' ,{['DB ' date]},...
%     'FitHeightToText','on');

end
