function [] = setSeed()
% seeds the random number generator with a hard-coded value for
% reproducibility
    rand('seed', 9786453120);
end