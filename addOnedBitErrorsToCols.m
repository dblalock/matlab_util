function mat = addOnedBitErrorsToCols(mat, errorsPerCol)
    [~, cols] = size(mat);
    for i=1:cols
        mat(:,i) = onedBitErrors(mat(:,i), errorsPerCol);
    end

    
    function vect = onedBitErrors(vect, numBits)
    % like AddBitErrorsInRange, but automatically just flips bits outside the 
    % block of ones. If the ones aren't all in a block, this will do bad
    % things.
    vectOnes = find(vect);
    onesStartIdx = vectOnes(1);
    numOnes = length(vectOnes);
    numZeros = length(vect) - numOnes;
    zerosVect = zeros(numZeros, 1);
    [~, errIndices] = AddBitErrorsInRange(zerosVect,numBits,1,numZeros);
    errIndices = errIndices + numOnes .* (errIndices >= onesStartIdx);
    vect(errIndices) = 1;
end

end