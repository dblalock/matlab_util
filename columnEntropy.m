function [entropy uniqColCounts uniqCols] = columnEntropy(A)
% [entropy uniqColCounts uniqCols] = columnEntropy(A)
%
% computes the Shannon entropy of a matrix A, where each column of A
% is treated as a symbol. That is, a matrix of n columns will
% be treated as a message of n symbols, and identical columns will
% be treated as identical symbols.
%
% This function is basically for computing the entropy of input and output
% firing patterns in neural networks.

[~, numCols] = size(A);

% Our technique for finding the counts is to sort them all and then see
% how many in a row are the same.
sortedA = sortrows(A')';
uniqColCounts = [];
uniqCols = [];
lastVect = not(A(:,1)); % can't match 1st col or 1st symbol count will be off by 1
for i=1:numCols
    if (sortedA(:,i) == lastVect)
        uniqColCounts(end) = uniqColCounts(end) + 1;
    else
        uniqColCounts = [uniqColCounts 1];
        uniqCols = [uniqCols sortedA(:,i)];
    end
    lastVect = sortedA(:,i);
end

% use our entropy function to calculate output entropy
entropy = shannonEntropy(uniqColCounts);

end