function [data] = shuffleRows(M)
   % randomly shuffles the order in which the rows of matrix M
   % appear
   [numRows, ~] = size(M);
   data = M(randperm(numRows), :);
end