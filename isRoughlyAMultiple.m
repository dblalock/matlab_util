function retval = isRoughlyAMultiple(a,b,k)
% retval = isRoughlyAMultiple(a,b, k)
%	compute whether a is roughly a multiple of b, where roughly
%  means that the difference between a/b and the nearest multiple
%  of b is less than b*k.
%
% Ex:
%
% >> isRoughlyAMultiple(5.75, 2, .125)
% 
% ans =
% 
%      1
% 
% >> isRoughlyAMultiple(5.75, 2, .124)
%
% ans =
% 
%      0

quotient = a / b;
err = (quotient - round(quotient));
% remainder = decimal * b
% err = decimal / quotient
retval = abs(err) <= k;

end