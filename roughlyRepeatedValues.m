function [newValues newIndices originalValues originalIndices] = roughlyRepeatedValues(X1, X2, tolerance)
% Given two vectors of real numbers X1 and X2, returns the values of X2
% that are within +/- tolerance times any value in X1, inclusive. 
%
% I.e., for tolerance = .1, this function returns all values in X2 that are 
% within +/-10% of some value in X1.
%
% EDIT: also returns the original values of which each of these returned
% values is "roughly a repeat". If it's a repeat of more than one, the closest
% (in terms of L1 distance) is used.
%
% EDIT2: also returns the indices within unique(X1, 'stable') of each of these
% original values. 
%
% EDIT3: also returns these indices for the new values in the same manner
%
% Example:
%
% >> [repVals repIndices origVals origIndices] = roughlyRepeatedValues([2 1 3], [.75 .8 1.2 .8 1.5 2.1], .2)
% 
% repVals =
% 
%     0.8000
%     1.2000
%     2.1000
% 
% repIndices =
% 
%      2
%      3
%      5
% 
% origVals =
% 
%      1
%      1
%      2
% 
% origIndices =
% 
%      2
%      2
%      1


% remove redundant entries
X1 = unique(X1, 'stable');
X2 = unique(X2, 'stable');

% find the range of acceptable values for each entry in X1
margins = tolerance * X1;

% now just check each entry in X2 to see if it falls in one of these ranges
len2 = length(X2);
newValues = cell(len2);
newIndices = cell(len2);
originalValues = cell(len2);
originalIndices = cell(len2);
for i=1:len2
    val = X2(i);
	diffs = abs(X1 - val);
	diffs = diffs ./ margins;
	[minDiff, idx] = min(abs(diffs));
	if (minDiff <= 1) % within one margin
       newValues{i} = val;
       newIndices{i} = i;
       originalValues{i} = X1(idx);
       originalIndices{i} = idx;
    end
end

    % quick inline function to add some readability
    function c = trimEmptyCells(c)
        c(cellfun(@isempty,c)) = [];
    end

% return a vector instead of a cell
newValues 		= cell2mat(trimEmptyCells(newValues))';
newIndices 		= cell2mat(trimEmptyCells(newIndices))';
originalValues 	= cell2mat(trimEmptyCells(originalValues))';
originalIndices = cell2mat(trimEmptyCells(originalIndices))';

end