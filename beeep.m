function [] = beeep(n)
SetDefaultValue(1,'n',10);

% vol = SoundVolume(.8);
s = beep;
beep on

for i=1:n
    beep;
    pause(.1);
end

% SoundVolume(vol);
cmd = ['beep ',s];
eval(cmd);

end