function [] = horizontalLine(y, width)
% verticalLine(x, height)
% Draws a vertical line in the current figure at the specified x value with
% the specified height
SetDefaultValue(2,'width',1);

line([0 width], [y y]);

end