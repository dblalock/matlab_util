function [lines] = fileCountInDir(dir)
    
    cmd=['ls -l ', dir, ' | wc -l'];
    [status, ret] = system(cmd);
    lines = str2num(ret)-1;

end