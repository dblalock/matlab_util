function dists = euclideanDists(A,B)
% dists = euclideanDists(A,B)
%
% returns a matrix containing the euclidean distances
% between each column of A and each column of B. Rows
% are values for each of A's columns and columns are
% values for each of B's columns.

[rowsA, colsA] = size(A);
[rowsB, colsB] = size(B);

% ensure columns are in same dimensional space
if (rowsA ~= rowsB)
   error(['euclideanDists(): A contians ',num2str(rowsA),...
       ' rows, but B contains ',num2str(rowsB)]) 
end

dists = zeros(colsA, colsB);

for i=1:colsA
    aCol = A(:,i);
    aColMat = repmat(aCol, 1,colsB);
    distances = B - aColMat;                % find L1 distances
    distances = distances .* distances;     % square them
    dists(i,:) = sqrt(sum(distances) );     % sqrts of sums of squares
end

end