function [pkVals pkIdxs pkAges] = updatedPeaks(oldPkVals, newPkVals, ...
	oldPkIdxs, newPkIdxs, oldPkAges, idxTolerance, epsilon, pkValCutoff)
% This function is basically designed to follow ridges. It computes
% a moving average of the values of their peaks, with the added power
% of considering peaks with indices within idxTolerance of one another
% to be continuations of the same ridge. It also tracks the "ages" of
% ridges by adding one to all pre-existing ridges and setting an "age"
% of one for all newly discovered ridges.
%
% TODO explain this much, much more thoroughly if anyone else will see
% this code; it's pretty darn hard to understand until you visualize it
% right.
%
% Example:
%
% >> [pkVals pkIdxs pkAges] = updatedPeaks([.01 .9 .1 .5],[.2 .45 .6], [0 9 1 5],[2 4.5 6], ...
% [10,11,12,13], .2, .5, .15)
% 
% pkVals =
% 
%     0.2500
%     0.4750
%     0.5500
%     0.4500
% 
% pkIdxs =
% 
%     2.0000
%     4.5000
%     6.0000
%     9.0000
% 
% pkAges =
% 
%      1
%     14
%     14
%     12

	% ensure everything is column vectors for uniformity
	oldPkVals = oldPkVals(:);
	newPkVals = newPkVals(:);
	oldPkIdxs = oldPkIdxs(:);
	newPkIdxs = newPkIdxs(:);
	oldPkAges = oldPkAges(:);
	
	% confusing step: the repeated values are the indices, and the 
	% positions returned are those within the arrays of indices we 
	% passed in.
	[childNewIdxs, childNewIdxPositions, ~, parentOldIdxPositions] = ...
		roughlyRepeatedValues(oldPkIdxs, newPkIdxs, idxTolerance);
	
	% -------------------------------
	% Repeated peaks
	% -------------------------------
	
	% the above told us which peaks were (rougly) repeated. Now use this
	% information to find the values of the peaks that were repeated.
	childNewVals = newPkVals(childNewIdxPositions);
	parentOldVals = oldPkVals(parentOldIdxPositions);
	updatedDescendantVals = (1-epsilon)*parentOldVals + epsilon*childNewVals;
	
	% using this same information, add to the ages of the repeated peaks
	descendantAges = oldPkAges(parentOldIdxPositions);
	descendantAges = descendantAges + 1;

	% -------------------------------
	% Absent peaks
	% -------------------------------

	% some peaks weren't repeated, but may still be worth tracking. Ie,
	% sometimes a peak will go away for one or two time steps, but will
	% quickly come back, and thus shouldn't be discarded
	
	% the absent indices are those that were in the old peaks,
	% but weren't repeated by the new peaks
	possibleOldPositions = 1:length(oldPkIdxs);
	unusedOldPositions = setdiff(possibleOldPositions, parentOldIdxPositions);
	absentOldIdxs = oldPkIdxs(unusedOldPositions);
	absentOldVals = oldPkVals(unusedOldPositions);
	updatedAbsentVals = (1-epsilon)*absentOldVals; %+ epsilon * 0, since no new val
	
	% using this same information, add to the ages of the absent peaks
	absentAges = oldPkAges(unusedOldPositions);
	absentAges = absentAges + 1;
	
	% -------------------------------
	% Novel peaks
	% -------------------------------
	
	% some peaks are completely new in the current set of peaks
	possibleNewPositions = 1:length(newPkIdxs);
	novelNewPositions = setdiff(possibleNewPositions, childNewIdxPositions);
	novelIdxs = newPkIdxs(novelNewPositions);
	novelVals = newPkVals(novelNewPositions);
	updatedNovelVals = novelVals * epsilon + pkValCutoff;	% no 1-epsilon term cuz no original value
		% offset by cutoff so it can't immediately get removed
		
	% set the age of new peaks to be 1
	novelAges = ones(length(updatedNovelVals), 1);
	
	% -------------------------------
	% Synthesize final peaks
	% -------------------------------
	
	% combine peaks from the three different conditions
	pkVals = [updatedDescendantVals; updatedAbsentVals; updatedNovelVals];
	pkIdxs = [childNewIdxs; absentOldIdxs; novelIdxs];
	pkAges = [descendantAges; absentAges; novelAges];
	
	% prune peaks that have are no longer above the cutoff
	idxsAboveCutoff = find(pkVals >= pkValCutoff);
	pkVals = pkVals(idxsAboveCutoff);
	pkIdxs = pkIdxs(idxsAboveCutoff);
	pkAges = pkAges(idxsAboveCutoff);
	
	% return values such that indices are in ascending order
	[pkIdxs, sortOrder] = sort(pkIdxs, 'ascend');
	pkVals = pkVals(sortOrder);
	pkAges = pkAges(sortOrder);
end