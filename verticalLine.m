function [] = verticalLine(x, height)
% verticalLine(x, height)
% Draws a vertical line in the current figure at the specified x value with
% the specified height
SetDefaultValue(2,'height',1);

line([x x], [0 height]);

end