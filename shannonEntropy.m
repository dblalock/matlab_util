function entropy = shannonEntropy(symbolCounts)
% computes the shannon entropy of a message given a vector describing
% the number of times each symbol appears

p = symbolCounts ./ sum(symbolCounts);
entropy = -sum(p .* log2(p));

end