function mat = addBitErrorsToCols(mat, errorsPerCol)
    [~, cols] = size(mat);
    for i=1:cols
        mat(:,i) = addBitErrors(mat(:,i), errorsPerCol);
    end

    function [vect errIndices] = addBitErrors(vect, numBits)
    % just calls AddBitErrorsInRange() with the start and end indices
        [vect errIndices] = AddBitErrorsInRange(vect, numBits, 1, length(vect) );
    end

end