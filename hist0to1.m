function [binVals, binPositions] = hist0to1(data, bins)
% Returns the bin values and positions given a data set
% and desired number of bins.
% Ensures that the bin positions are all within 0 to 1.

binVals = hist(data, bins);
binPositions = (.5:bins-.5)./bins;

end