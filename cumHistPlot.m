function [binVals, binPositions] = cumHistPlot(data, bins, titleStr, xLabel, yLabel)
% Creates a cumulative histogram with the specified data, bin count, title, 
% xlabel and ylabel
% Note that bar magnitudes are scaled such that the final value is 1

% create bins from data, with bins positioned from 0 to 1
[binVals, binPositions] = hist0to1(data, bins);
    
% make cumulative
binVals = cumsum(binVals);

% actually create the histogram
bar(binPositions, binVals);
title(titleStr);
xlabel(xLabel);
ylabel(yLabel);

end