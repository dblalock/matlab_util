function cosTheta = cosineCompareMats(A, B)
% returns |u . v| / (|u|*|v|), where u and v are vectors consisting of
% all of the values in A and B

dotProd = sum( sum(A .* B) );   % |u . v|
dotProdSq = dotProd * dotProd;
dotProdSign = sign(dotProd);
% mag = sqrt( sum( sum(A.*A) ) ) * sqrt( sum( sum(B.*B) ) );  % (|u|*|v|)
magSq = sum( sum(A.*A) ) * sum( sum(B.*B) );  % (|u|*|v|)
% mag = norm(A) * norm(B);        % (|u|*|v|)   % this computes matrix norms,
                                                % which are bad.
% cosTheta = dotProd / mag       % |u . v| / (|u|*|v|)
cosThetaSq = dotProdSq / magSq;       % |u . v| / (|u|*|v|)
cosTheta = dotProdSign * sqrt(cosThetaSq);

end