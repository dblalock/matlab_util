function quantizedMat = quantizeAtLevels(A, levelsVect)
% Given a matrix A, returns a matrix B such that B(i,j) is equal
% to the number of levels within levelsVect that the A(i,j) is
% greater than or equal to, divided by the number of levels
% specified, so that the range of possible values is [0,1].
%
% Basically, this function lets you preprocess a matrix for
% imagesc so that there will be clear boundaries around any
% breakpoints you care about. You might quantize it at, say
% [0 .5] if you just wanted to see areas less than 0, between
% 0 and .5, and greater than .5

	lvls = unique(levelsVect);	% ensure levels unique and in ascending order
	
	numThresholds = length(lvls);
	
	[r, c] = size(A);
	
	quantizedMat = zeros(r,c);
	for i=1:numThresholds
		thresh = lvls(i);
		quantizedMat = quantizedMat + (A >= thresh);
	end
	
	% normalize to 1 as max value
	quantizedMat = quantizedMat ./ numThresholds;
end
