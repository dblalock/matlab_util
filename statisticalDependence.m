function dep = statisticalDependence(X)
% Given a matrix with columns representing distinct symbols, 
% computes Sum(H(Xi)) - H(X)

[~, xCols] = size(X);

% get the entropy for the whole set of codewords (columns)
H_x = columnEntropy(X);

% now find the entropies for each feature of X (rows)
sums = sum(X, 2);
% sums = sum(X)   % let's try it with cols...
probabilities = sums / xCols;
individualEntropies = bernoulliEntropy(probabilities);
sum_H_Xi = sum(individualEntropies);
% sum_H_Xi = -log2( prod(probabilities) );

% sum_H_Xi = columnEntropy(X')
% 
% sums2 = sum(X);
% probabilities2 = sums2 / xRows;
% H_x2 = -log2( prod(probabilities2) );

% statistical dependence is the difference between these two values
dep = sum_H_Xi - H_x;

end