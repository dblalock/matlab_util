function [ binVals, binPositions] = cumRelHistPlot(data, bins, titleStr, xLabel, yLabel, unitWidth)
% Creates a cumulative histogram with the specified data, bin count, title, 
% xlabel and ylabel
% Note that bar magnitudes are scaled such that the final value is 1
% 
% If unitWidth is set to true, then horizontal axis will range from 0 to 1
SetDefaultValue(6, 'unitWidth', 0)

% create bins from data, with bins positioned according to unitWidth
% parameter
if (unitWidth)
    [binVals, binPositions] = hist0to1(data, bins);
else
    [binVals, binPositions] = hist(data, bins);
end
    
% make cumulative
binVals = cumsum(binVals);
    
% scale to 1
binVals = binVals / binVals(end);

% actually create the histogram
bar(binPositions, binVals);
title(titleStr);
xlabel(xLabel);
ylabel(yLabel);
addMyName()

end