function fileNamesCell = ls2Cell(str)
% ls() returns a giant string, which is not terribly useful. This function
% returns a cell instead.

fileNamesStr = ls('-1',str);                % force single-column
fileNamesCell = regexp(fileNamesStr,'\n','split');
fileNamesCell(end) = [];  % trailing \n yields empty entry


% numResults = length(fileNamesCell) -1;  % trailing \n yields empty entry
% fileNames = zeros(numResults, 1);

% populate the vector with the results returned by the split operation;
% we can't just use cell2mat because it just concatenates all the strings
% for i=1:numResults
%     fName = fileNamesCell{i}
%     fileNames(i, :) =  fName;
% end

end