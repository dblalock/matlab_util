function B = repColVectFast(vect, reps)
% returns a matrix composed of reps columns each equal to vect
    B = vect(:, ones(reps, 1));
%     B = zeros(vectLength, matrixCols);
%     for i=1:matrixCols
%        B(:,i) = vect; 
%     end
%     B(vect(ones(vectLength, 1), :)
end