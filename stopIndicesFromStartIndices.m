function stopIndices = stopIndicesFromStartIndices(startIndices, maxIndex)
% given a vector of indices in which categories start, returns a vector of
% the indices in which they stop
%
% i.e., [1 5 7] with a max index of 10 returns [4 6 10]

indexCount = length(startIndices);
stopIndices = zeros(1, indexCount);
for i=1:(indexCount-1)
    stopIndices(i) = startIndices(i+1) - 1;
end
stopIndices(end) = maxIndex;

end