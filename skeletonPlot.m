function [] = skeletonPlot( xdata, ydata, titleStr, xStr, yStr, format, tight, lineWidth)
% xdata: column vector of xaxis data
% ydata: column vector of yaxis data
% titleStr: string to display as plot title
% xStr: string with which to label x axis
% yStr: string with which to label y axis
% format: string to pass into plot command to alter display (eg, 'r+')
% tight: if set, axes are fit tightly to the data

SetDefaultValue(3,'titleStr','X vs Y');
SetDefaultValue(4,'xStr','x');
SetDefaultValue(5,'yStr','y');
SetDefaultValue(6,'format','.');
SetDefaultValue(7,'tight',0);
SetDefaultValue(7,'lineWidth',1);

% error trapping
xdata = colVect(xdata);
ydata = colVect(ydata);

% Create figure
figure1 = figure('color',[1,1,1]);

% replace MatLab's gray border with white
colormap gray

% Create axes
% the default MatLab axis is aesthetically inferior because it plots points
% on the figure edges. Here we will force the axes beyond all data points.
axes1 = axes('Parent',figure1);
if (tight)
    axis(axes1, [min(xdata) , max(xdata), ...
    min(ydata) , max(ydata)], 'square');
else
    axis(axes1, [min([xdata-0.05;0]) , max([xdata+0.05;1]), ...
    min([ydata-0.1;0]) , max([ydata+0.1;1])], 'square');
end

% forcing the axis larger
title(axes1,titleStr);
xlabel(axes1,xStr);
ylabel(axes1,yStr);
box(axes1,'on');
hold(axes1,'all');

% Create plot
plot(xdata,ydata,format, 'LineWidth',lineWidth);

% Annotate with my name and the date
addMyName()

end