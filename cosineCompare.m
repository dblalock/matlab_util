function mat = cosineCompare(M)
% returns a matrix consisting of the cosine comparison of the columns of A
% note that NaN entries are replaced with 0
% 
% Note to self: the function cosineComp() in ../project is more efficient

A = full(double(M));

mat = (A')*A;
[rows, cols] = size(A);
magnitudes = zeros(1,cols);    % magnitudes of each col as a row vect
for i=1:cols
   magnitudes(i) = norm(A(:,i));
end
normalizations = magnitudes' * magnitudes; % outer product of magnitudes
mat = mat ./ normalizations;
mat(isnan(mat)) = 0;

end