function [] = labelFigure(titleStr, xStr, yStr)
% creates a figure and labels it with the title, x label, 
% and y label provided. Also annotates it with my name 
% and the date.

title(titleStr)
xlabel(xStr)
ylabel(yStr)
addMyName()

end