function B = repRowVectFast(vect, reps)
% returns a matrix composed of reps rows each equal to vect
    B = vect(ones(reps, 1), :);
end