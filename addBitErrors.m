function [vect errIndices] = addBitErrors(vect, numBits)
% just calls AddBitErrorsInRange() with the start and end indices
    [vect errIndices] = AddBitErrorsInRange(vect, numBits, 1, length(vect) );
end