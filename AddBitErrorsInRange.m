function [vect usedIndices] = AddBitErrorsInRange(vect, numBits, rangeStart, rangeStop)
% given a binary vector, a number of bits to flip, and a range of indices
% eligible to be flipped, returns a bit-error-ed copy of the original
% vector
% also returns the indices that were flipped as a second retval
    usedIndices = ones(1,numBits)* -1;  % negative so won't match initially
    flipIndex = -1;
    rangeSize = rangeStop - rangeStart + 1;
    if (numBits > rangeSize)
        disp(['ERR: AddBitErrorsInRange: cant flip that many bits: ', num2str(numBits)])
        return
    end

    for bitNum=1:numBits
        while( any( usedIndices == flipIndex) )
            flipIndex = floor(rand(1)*rangeSize + rangeStart);
        end
        usedIndices(bitNum) = flipIndex;
        vect(flipIndex) = ~vect(flipIndex);
    end
end