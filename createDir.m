function [] = createDir(dir)
%Creates the specified directory, clobbering it if it exists
    if exist(dir,'dir')
        rmdir(dir,'s');
    end
    mkdir(dir);
end