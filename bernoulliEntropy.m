function bits = bernoulliEntropy(p)
% compute entropy for a single bernoulli trial with a given probability
    q = 1-p;
    bits = -( p.*log2(p) + q.*log2(q) );
    bits( isnan(bits) ) = 0;
end