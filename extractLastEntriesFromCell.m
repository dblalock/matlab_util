function vect = extractLastEntriesFromCell(C, N)
    % returns a column vector containing the last N entries
    % from cell C
    vect = zeros(N,1);
    for idx=1:N
        vect(idx) = C{end - idx + 1};
    end
end