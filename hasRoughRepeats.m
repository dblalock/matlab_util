function B = hasRoughRepeats(refs, candidates, tolerance)
% B = hasRoughRepeats(refs, candidates, tolerance)
%
% Checks whether any of the values in candidates are within 
% tolerance of any of the values in refs

repeatedValues = roughlyRepeatedValues(refs, candidates, tolerance);
B = ~(isempty(repeatedValues));

end