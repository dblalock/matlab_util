function [mn, vr, stdev] = stats(dataSet)
    mn = mean(dataSet);
    vr = var(dataSet);
    stdev = std(dataSet);
end