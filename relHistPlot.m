function [ binVals, binPositions] = relHistPlot(data, bins, titleStr, xLabel, yLabel, unitWidth)
% Creates a histogram with the specified data, bin count, title, xlabel
% and ylabel
% Note that bar magnitudes are scaled such that they sum to 1
% 
% If unitWidth is set to true, then horizontal axis will range from 0 to 1
SetDefaultValue(6, 'unitWidth', 0)

% create bins from data, with bins positioned according to unitWidth
% parameter
if (unitWidth)
    [binVals, binPositions] = hist0to1(data, bins);
else
    [binVals, binPositions] = hist(data, bins);
end

% scale to 1
binVals = binVals / sum(abs(binVals));
    
% actually make bar graph; note that x always 0-1
bar(binPositions, binVals);
title(titleStr)
xlabel(xLabel)
ylabel(yLabel)
addMyName()

end