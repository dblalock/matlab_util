function paddedMat = cell2matWithPadding(c)
% cell2mat can only operate on cells whose entries are of uniform length;
% this function pads a cell of vectors such that all entries are the same
% length (by adding trailing zeros).
    len = length(c);

    % figure out the max length of a column so we can add padding and
    % use cell2mat
    maxRows = 0;
    for i=1:len
        n = length(c{i});
        if n > maxRows
            maxRows = n;
        end
    end

    % actually add the padding
    for i=1:len
        paddingLen = maxRows - length( c{i} );
        c{i} = [c{i}; zeros(paddingLen, 1)];
    end

    % convert to matrix and then surf the matrix
    paddedMat = cell2mat(c);
end