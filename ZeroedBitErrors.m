function errVect = ZeroedBitErrors(vect, numBits)
% like AddBitErrorsInRange, but automatically just flips bits in the 
% block of ones. If the ones aren't all in a block, this will do bad
% things.
    vectOnes = find(vect);
    errVect = AddBitErrorsInRange(vect,numBits,vectOnes(1), vectOnes(end));
end