function symbols = cols2symbols(A)
% symbols = cols2symbols(A)
%
% Converts unique columns to unique symbols. I.e., if each column
% is a codeword, then these codewords are numbered and the returned
% row vector is the number assigned to each column.
%
% Example:
%
% >> x = [4 4 2 1 1 4];
% >> cols2symbols(x)
% 
% ans =
% 
%      1     1     2     3     3     1

[~,~,symbols] = unique(A', 'rows', 'stable');
symbols = symbols';

end